﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	//public parameters with default values
	public float minSpeed = 1.0f;
	public float maxSpeed = 5.0f;
	public float minTurnSpeed = 1.0f;
	public float maxTurnSpeed = 5.0f;
	public ParticleSystem explosionPrefab;

	//private state
	private float speed;
	private float turnSpeed;
	//public Transform target1;
	//public Transform target2;
	private Transform target;
	private Vector2 heading;

	void Start() {
		//find a player object to be the target by type
		PlayerMove player = FindObjectOfType<PlayerMove>();
		target = player.transform;

		//bee initially moves in random direction
		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate(angle);

		//set speed and turnSpeed randomly
		speed = Mathf.Lerp (minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp (minTurnSpeed, maxTurnSpeed, Random.value);
	}

	void Update () {
		//get the vector from the bee to the targets
		Vector2 direction = target.position - transform.position;
		//Vector2 direction2 = target2.position - transform.position;

		//calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		//follow closest player
		//if (direction1.magnitude < direction2.magnitude) {
		//	target = target1;
		//} else {
		//	target = target2;
		//}

		//Get the vector to the current target
		//Vector2 direction = target.position - transform.position;

		//turn left or right
		if (direction.IsOnLeft(heading)) {
			//target on the left, rotate anticlockwise
			heading = heading.Rotate(angle);
		} else {
			//target on the right, rotate clockwise
			heading = heading.Rotate(-angle);
		}
			
		transform.Translate (heading * speed * Time.deltaTime);
	}

	void OnDestroy () {
		//create an explosion at the bee's current position
		ParticleSystem explosion = Instantiate(explosionPrefab);
		explosion.transform.position = transform.position;
		//destroy the particle system when it's over
		Destroy(explosion.gameObject, explosion.duration);
	}

	//void OnDrawGizmos() {
		//draw heading vector in red
		//Gizmos.color = Color.red;
		//Gizmos.DrawRay (transform.position, heading);

		//draw target vector in yellow
		//Gizmos.color = Color.yellow;
		//Vector2 direction = target.position - transform.position;
		//Gizmos.DrawRay (transform.position, direction);
	//}
}
